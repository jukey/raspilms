## Idea

Run the Logitech Media Server (LMS) Community Edition on a RaspberryPie or 
another computer running linux.

This is just a collection of links and todos that helped me to switch my 
Squeezeboxes to LMS in order to have support for Spotify again. The official 
service provided by logitech (mysqueezebox.com) stopped offering support for 
Spotify recently due to changes in the protocol by Spotify. Thankfully there
was a new, working [plugin called **Spotty**](http://forums.slimdevices.com/showthread.php?107418-The-Spotty-Spotify-implementation)
created by Michael Herger


## Installation

I tested the installation on a Laptop running Ubuntu Gnome 17.04 first.
Secondly I did the same on a RaspberryPie 3 Model B.

### RaspberryPie

- [Raspbian Download page](https://www.raspberrypi.org/downloads/raspbian/)
- [Installation Guide and Raspbian manual](https://www.raspberrypi.org/documentation/raspbian/)

### Server

The server could be the RaspberryPie or another computer running Ubuntu/Debian Linux.

- In order to avoid an error (*"Perl need module `IO::Socket::SSL`"*) one need to install the related package for ubuntu: `sudo apt-get install libio-socket-ssl-perl`
- [Installation Script](http://wiki.slimdevices.com/index.php/DebianPackage#installing_7.9.0)
- http://localhost:9000/settings/
- In the 3rd Party Plugin Section
  - Enable: **Spotty** Spotify Plugin
  - Disable: **Official** Spotify Plugin
- Restart Server:

```bash
sudo /etc/init.d/logitechmediaserver restart
```

- Select [Plugin Settings for Spotty](http://localhost:9000/plugins/Extensions/settings/plugins/Spotty/settings/basic.html)
- (*Sometimes* an error message appears: `404 Not Found: plugins/Extensions/settings/plugins/Spotty/settings/basic.html`. In this case select: Setting / Advanced / Spotty ([See bug report](https://github.com/michaelherger/spotty/issues/8))
- Follow the instructions in order to connect your LMS and Spotify

### Squeezebox Radio

- Turn it on
- Select "Eigene Musicsammlung". This switches to the squeezebox server within the same WiFi Network.
- Select "Eigene Anwendungen" --> Spotify

## Access the LMS

### Via Browser

- http://localhost:9000/settings/

### Via CLI (Terminal)

```bash
# Start Server

sudo service logitechmediaserver start

# Stop Server

sudo service logitechmediaserver start

# Restart Server

sudo service logitechmediaserver start

```

### Via Squeezebox Radio

- In the main menue switch to "Eigene Musiksammlung" and select the raspberrypi as server


### How it looks like

![raspilms](raspilms.jpg)

## FAQ

### How I prevent starting the logitechmediaserver when system is booting?

To remove the service from being started while booting do:

```bash
sudo update-rc.d logitechmediaserver remove
```

In order to activate start while booting again:

```bash
sudo update-rc.d mysql defaults
```

## Resources

- [Download Logitech Media Server Community Edition 7.9](http://downloads.slimdevices.com/nightly/?ver=7.9)
- [Changelog LMS Community Edition 7.9](http://htmlpreview.github.io/?https://raw.githubusercontent.com/Logitech/slimserver/public/7.9/Changelog7.html)
- [Spotty Source Code Repository at github](https://github.com/michaelherger/spotty)
- [A Guide how to set up a virtual RaspberryPie using VirtualBox](https://grantwinney.com/how-to-create-a-raspberry-pi-virtual-machine-vm-in-virtualbox/)

## My other Raspberry Pi Project

- [raspiclock](https://gitlab.com/jukey/raspiclock) - Make you Raspberry Pi an artful and fancy clock using a screen and the wonderful Standard Time project